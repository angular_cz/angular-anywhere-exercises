import { Component } from '@angular/core';

@Component({
  selector: 'app-container',
  templateUrl: 'container.component.html',
  styleUrls: ['./container.component.css'],
  // TODO 2.4 - registrujte službu na úrovni komponenty
  providers: []
})
export class ContainerComponent {
}
